import React from "react";

const Layout = ({children}) => {
  return (
    <div className="reveal">
      <div className="slides">{children}</div>
    </div>
  );
};

export default Layout;
