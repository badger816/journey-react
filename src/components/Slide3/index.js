import React from "react";

const Slide3 = (props) => {
  const {background} = props;
  return (
    <section data-background={background} data-transition="concave">
      <h3>Thoughts on Digital Development</h3>
      <ul>
        <li className="fragment fade-left">Low-Fidelity Prototyping</li>
        <li className="fragment fade-left">High-Fidelity Prototyping</li>
        <li className="fragment fade-left">Mobile First</li>
        <li className="fragment fade-left">Usability</li>
        <li className="fragment fade-left">
          Picking the best tools for the job
        </li>
        <li className="fragment fade-left">Moduler Code</li>
        <li className="fragment fade-left">Decoupled architecture</li>
      </ul>
    </section>
  );
};

export default Slide3;
