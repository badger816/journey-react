import React from "react";
import VideoFile from "../../assets/video/hotwire.mp4";
import styles from "../../assets/styles/global.scss";

const Hotwire = () => {
  return (
    <section
      data-transition="concave"
      data-background-video={VideoFile}
      data-background-video-loop="true"
      data-background-video-muted="true"
      data-background-opacity="0.5"
    >
      <h1 className={styles.shadow}>Hotwire</h1>
    </section>
  );
};

export default Hotwire;
