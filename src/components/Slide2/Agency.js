import React from "react";
import VideoFile from "../../assets/video/meds-vid.mp4";
import styles from "./agency.module.scss";

const MedsAgency = () => {
  return (
    <section
      data-transition="concave"
      data-background-video={VideoFile}
      data-background-video-loop="true"
      data-background-video-muted="true"
      data-background-opacity="0.5"
    >
      <h2 className={styles.shadow}>Medicine Agency</h2>
    </section>
  );
};

export default MedsAgency;
