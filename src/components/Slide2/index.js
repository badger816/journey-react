import React from "react";
import Agency from "./Agency";
import Isobar from "./Isobar";
import Sony from "./Sony";
import Hotwire from "./Hotwire";
import Samsung from "./Samsung";

const Slide2 = (props) => {
  const {background} = props;
  return (
    <section data-background={background} data-transition="concave">
      <section data-transition="concave">
        <h2>My Background</h2>
      </section>
      <Agency />
      <Isobar />
      <Hotwire />
      <Sony />
      <Samsung />
    </section>
  );
};

export default Slide2;
