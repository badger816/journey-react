import React from "react";
import VideoFile from "../../assets/video/isobar.mp4";
import styles from "../../assets/styles/global.scss";

const Isobar = () => {
  return (
    <section
      data-transition="concave"
      data-background-video={VideoFile}
      data-background-video-loop="true"
      data-background-video-muted="true"
      data-background-opacity="0.5"
    >
      <h1 className={styles.shadow}>Isobar</h1>
    </section>
  );
};

export default Isobar;
