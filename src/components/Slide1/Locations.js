import React from "react";
import styles from "./locations.module.scss";

const Locations = () => {
  return (
    <>
      <section data-background="#656571" data-transition="concave">
        <h2 className={styles.shadow}>Places I have lived</h2>
        <p className="fragment fade-right">Minneapolis, MN</p>
        <p className="fragment fade-left">Boulder, CO</p>
        <p className="fragment fade-right">Minneapolis, MN</p>
        <p className="fragment fade-left">Oakland, CA</p>
      </section>
    </>
  );
};

export default Locations;
