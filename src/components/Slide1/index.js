import React from "react";
import Locations from "./Locations";
import Sports from "./Sports";
import styles from "./slide1.module.scss";

const Slide1 = (props) => {
  const {background} = props;
  return (
    <section data-background={background}>
      <section data-transition="concave">
        <h1 className={styles.title}>Kyle Ross</h1>
        <p className={`fragment ${styles.shadow}`}>Web Developer</p>
        <p className={`fragment ${styles.shadow}`}>Pinole, CA</p>
      </section>
      <Locations />
      <Sports />
    </section>
  );
};

export default Slide1;
