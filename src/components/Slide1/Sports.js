import React from "react";
import styles from "./sports.module.scss";
import GolfVideo from "../../assets/video/golf.MP4";
import SkateVideo from "../../assets/video/skatevid.mp4";
import Ghin from "../../assets/images/ghin.jpg";
const Sports = () => {
  return (
    <>
      <section data-transition="concave">
        <h2 className={styles.shadow}>Interests</h2>
      </section>
      <section
        data-background="#0f3460"
        data-background-video={SkateVideo}
        data-background-video-loop="true"
        data-background-video-muted="true"
        data-background-opacity="0.5"
        data-transition="concave"
      >
        <p>Skateboarding</p>
        <ul>
          <li className="fragment">20+ years</li>
          <li className="fragment">
            Traveled the country doing demos and contests
          </li>
          <li className="fragment">Exposed to a lot of creative people</li>
        </ul>
      </section>
      <section
        data-background-video={GolfVideo}
        data-background-video-loop="true"
        data-background-video-muted="true"
        data-background-opacity="0.5"
        data-transition="concave"
      >
        <p>Golf</p>
        <ul>
          <li className="fragment">
            Learned golf from EA's Tiger Woods video game
          </li>
          <li className="fragment">Found it similar to skateboarding</li>
          <li className="fragment">I enjoy playing golf tournaments</li>
          <li className="fragment">Currently a 5.6 handicap</li>
          <li className="fragment" style={{listStyle: "none"}}>
            <img src={Ghin} alt="golf card" />
          </li>
        </ul>
      </section>
    </>
  );
};

export default Sports;
