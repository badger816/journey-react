import React from "react";
import styles from "./credits.module.scss";
import Avatar from "../../assets/images/avatar.jpg";
const Credits = () => {
  return (
    <section>
      <div className={styles.container}>
        <img className={styles.avatar} src={Avatar} alt="avatar" />
        <p>
          <a href="https://badger816.gitlab.io/journey-react">
            https://badger816.gitlab.io/journey-react
          </a>
        </p>
        <p>
          This presentaion was built using
          <a href="https://revealjs.com/"> Reveal JS</a>,
          <a href="https://reactjs.org/"> React Js</a> and hosted with
          <a href="https://docs.gitlab.com/ee/user/project/pages/">
            GitLab pages
          </a>
        </p>
      </div>
    </section>
  );
};

export default Credits;
