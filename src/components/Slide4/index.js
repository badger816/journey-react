import React from "react";
import Bell from "../../projects/bell";
import Hotwire from "../../projects/hotwire";

const Slide4 = (props) => {
  const {background} = props;
  return (
    <section data-background={background} data-transition="concave">
      <section>
        <h2>Real World Examples</h2>
      </section>
      <Bell />
      <Hotwire />
    </section>
  );
};

export default Slide4;
