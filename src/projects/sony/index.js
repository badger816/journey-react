import React from "react";

const Sony = () => {
  return (
    <>
      <section>
        <h2>Sony</h2>
        <ul>
          <li className="fragment fade-left">HTML, CSS, Javascipt</li>
          <li className="fragment fade-left">Full responsive</li>
        </ul>
      </section>
      <section
        data-background-iframe="https://www.sony.co.uk/"
        data-background-interactive
      ></section>
    </>
  );
};

export default Sony;
