import React from "react";
//import Image from "./images/homepage.png";
//import styles from "./bell.module.scss";

const Bell = () => {
  return (
    <>
      <section>
        <h2>Bell Flights</h2>
      </section>
      <section
        data-background-iframe="https://www.bellflight.com/"
        data-background-interactive
      ></section>
    </>
  );
};

export default Bell;
