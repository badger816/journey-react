import React from "react";

const Hotwire = () => {
  return (
    <>
      <section>
        <h2>hotwire</h2>
      </section>
      <section
        data-background-iframe="https://www.hotwire.com/hotels/"
        data-background-interactive
      ></section>
    </>
  );
};

export default Hotwire;
