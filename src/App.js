import React, {useEffect} from "react";
import Layout from "./templates/layout";
import Slide1 from "./components/Slide1";
import Slide2 from "./components/Slide2";
import Slide3 from "./components/Slide3";
import Slide4 from "./components/Slide4";
import Credits from "./components/Credits";
import Reveal from "reveal.js";
import "reveal.js/dist/reveal.css";
import "reveal.js/dist/theme/night.css";

function App() {
  useEffect(() => {
    Reveal.initialize();
  }, []);
  return (
    <Layout>
      <Slide1 background="#323141" />
      <Slide2 background="#4d4f76" />
      <Slide3 background="#ff8000" />
      <Slide4 background="#ec2600" />
      <Credits background="#3A06CC" />
    </Layout>
  );
}

export default App;
